Unix V6 Assembler Syntax Highligting
====================================

A rudimentary Vim syntax highlighting scheme for the
[Unix V6](https://en.wikipedia.org/wiki/Version_6_Unix)
assembler, `as`.

Copy the contents of the directories in this repository to `$VIMHOME`.
Filetype detection is not yet available; you’ll likely have to use a
[modeline](http://vimdoc.sourceforge.net/htmldoc/options.html#modeline).

Forward any bugs or suggestions to the
[issue tracker](https://gitlab.com/ssterling/v6as-vim/issues).
