" Vim syntax file
" Language: Ancient Unix ‘as’ PDP-11 Assembly Landuage
" Maintainer: Seth Price
" Latest Revision: 25 April 2021

" Quit if syntax file is already loaded
if exists("b:current_syntax")
	finish
endif

" To my knowledge, it’s case-insensitive
syn case ignore

" Allow ‘.’ in keywords, and disallow ‘:’ to be by keywords
set iskeyword=@,46,58

" Special PC instruction
syn match v6asPcDot "\v\."
hi def link v6asPcDot Identifier

" Constants
syn match v6asOctConst "\v[\*\$\#]*[0-7]+"
syn match v6asDecConst "\v[\*\$\#]*[0-9]+\." contains=v6asDecConstDel,v6asQual
syn match v6asCharConst "\v[\*\$\#]*\`." contains=v6asCharConstDel,v6asQual
syn match v6asDcharConst "\v[\*\$\#]*\".{2}" contains=v6asDcharConstDel,v6asQual
syn match v6asDecConstDel "\v\." contained
syn match v6asCharConstDel "\v\`" contained
syn match v6asDCharConstDel "\v\"" contained
hi def link v6asOctConst Number
hi def link v6asDecConst Number
hi def link v6asDecConstDel Special
hi def link v6asCharConst Number
hi def link v6asCharConstDel Special
hi def link v6asDcharConst Number
hi def link v6asDcharConstDel Special

" Reference/constant qualifiers
syn match v6asQual "\v[\*\$\#]" containedin=v6asOctConst,v6asDecConst,v6asCharConst,v6asDCharConst
hi def link v6asQual Operator

" Labels
syn match v6asLabel "\v^\s*[a-z0-9_]*:"he=e-1
hi def link v6asLabel Label

" Registers
syn match v6asRegister "\vf?r[0-5]"
syn match v6asRegister "sp"
syn match v6asRegister "pc"
hi def link v6asRegister Identifier

" ASCII strings
syn region v6asString start="\v\<" end="\v\>" contains=v6asStringEsc
syn match v6asStringEsc "\v\\[nte0rap\\\>]" contained
hi def link v6asString String
hi def link v6asStringEsc Special

" Pseudo-operands
syn keyword v6asPseudoOp .text .bss .byte .even .globl .comm .if .endif
syn region v6asIfRegion transparent matchgroup=v6asIf start="\v\.if" matchgroup=v6asEndIf end="\v\.endif" keepend
hi def link v6asPseudoOp PreProc
hi def link v6asIf PreCondit
hi def link v6asEndIf PreCondit

" Opcodes
syn keyword v6asOpcode clc clv clz cln
syn keyword v6asOpcode sec sev sez sen
syn keyword v6asOpcode br bne beq bge blt bgt ble bpl bmi bhi blos bvc bvs bhis bec bcc blo bcs bes
syn keyword v6asOpcode jbr jne jeq jge jlt jgt jle jpl jmi jhi jlos jvc jvs jhis jec jcc jlo jcs jes
syn keyword v6asOpcode clr com inc dec neg adc sbc ror rol asr asl jmp swab tst
syn keyword v6asOpcode clrb comb incb decb negb adcb sbcb rorb rolb asrb aslb tstb
syn keyword v6asOpcode mov cmp bit bic bis add sub movb cmpb bitb bicb bisb
syn keyword v6asOpcode jsr xor rts sys sxt mark sob
syn keyword v6asOpcode ash als ashc alsc mul mpy div dvd
syn keyword v6asOpcode jsr rts sys trap ash ashc mul div xor sxt mark sob
syn keyword v6asOpcode cfcc
syn keyword v6asOpcode setf clrf negf absf tstf movf addf subf mulf divf cmpf modf
syn keyword v6asOpcode setd seti setl
syn keyword v6asOpcode movif movfi movof movfo movie movei
syn keyword v6asOpcode ldfps stfps stst
syn keyword v6asOpcode halt
hi def link v6asOpcode Statement

" System calls
syn keyword v6asSysCall break chdir chmod chown close creat exec exit fork fstat
syn keyword v6asSysCall getuid gtty link makdir mdate mount nice open read seek
syn keyword v6asSysCall setuid signal stat stime stty tell time umount unlink wait write
hi def link v6asSysCall Constant

" Comments
setlocal comments="/"
syn match v6asComment "\v/.*$" contains=@Spell,v6asTodo
syn match v6asTodo "\v(TODO|FIXME|XXX|NOTE)" contained
hi def link v6asComment Comment
hi def link v6asTodo Todo
